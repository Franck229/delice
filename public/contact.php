<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap" rel="stylesheet">
    <style> @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap'); </style>
    <link rel="stylesheet" href="contact.css">
  <link rel="stylesheet" href="style.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body>
    
<nav class="navbar navbar-expand-md bg-dark">
        <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
        <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
            <i class="fa-solid fa-bars "></i>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="nav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="index.php">Accueil
                </a>
            </li>
            <li class="nav-item dropdown">
                <a 
                    class="nav-link text-light font-weight-bold px-3 dropdown-toggle" href="#" data-toggle="dropdown">Categorie
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="produit1.php">Chocolat</a>
                    <a class="dropdown-item" href="produit3.php">Miel</a>
                    <a class="dropdown-item" href="produit2.php">Carotte</a>
                </div>
            </li>
          
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="Apropos.php">A Propos
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="contact.php">Contactez-nous
                </a>
            </li>

        </ul>

            <form class="form-inline ml-3">
                <div class="input-group">
                    <input type="text"   style="border-radius: 8px;" placeholder="">
                    <div>
                        <button type="button " style="background-color:  rgb(255, 255, 13); border-radius: 8px;">Rechercher</button>
                    </div>
                </div>

            </form>
        </div>
    </nav>
    
    <div class="container ">
        <div class="momo">
            <h2 >Formulaire de contact </h2><br>
        </div>
       
        <div class="col-md-12 ">
            
               <div class="row">

                <div class="col-md-5 conn">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30523.079734740015!2d2.3684238317042516!3d6.
                    371690733545781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x10235503f4b7d715%3A0x10f5b13bb87bfa7a!2
                    sAbomey%20calavi!5e0!3m2!1sfr!2sbj!4v1672286946951!5m2!1sfr!2sbj" width="450" height="430" style="border:0;" 
                    allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>


                <div class=" col-md-5 con">
                       
                    <form action="" class="form">
                       
        
                        <div class="form-group">
                            <label for="Nom">Nom :</label>
                            <input type="text" id="Nom" class="form-control">
                        </div>
                        
                        <div class="form-group">
                            <label for="Nom">Prenom :</label>
                            <input type="text" id="Prenom" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="Prenom">Email :</label>
                           <input type="text" name="Email" class="form-control" id="">
                        </div>
        
                        <div class="form-group">
                            <label for="Commentaire">Message :</label>
                            <textarea class="form-control " id="Message"></textarea>
                        </div>
                    
                        <button type="submit" class=" btn-success btn" >Valider</button>
                       
                    </form>
                </div>

               </div>
               

            </div>
        </div>
       
    </div>




    <div class="conttainer-fluid">
        <div class="col-md-12 foot">
            <div class="row">
                <div class="col-md-3 sefoot">
                        <div class="row">
                            <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
                        </div>
                        <div class="row">
                            <p ">
                                Une patisserie qui vous fait goute
                                aux delices de l'art culinaire (patisserie) et qui satisfait tout vos fantasme sur l'art culinaire de la patisserie
                                 pour votre plus grand plaisir.
                            </p>
                           
                        </div>
                </div>

                <div class="col-md-3 sefoots">
                    <div class="row">
                        <h2>Liens rapide</h2>
                    </div>
                    <div class="row">
                        <div class="dernier">
                            <ul >
                                <li style="list-style: none; margin-left: -30px;"> <a href="#" style="text-decoration: none; color:white;"> Accueil </a></li>
                                <li style="list-style: none; margin-left: -30px;"><a href="inscription.php" style="text-decoration: none; color:white;"> Inscription </a></li>
                                <li style="list-style: none; margin-left: -30px;"><a href="connexion.php" style="text-decoration: none; color:white;"> connexion </a></li>
                                <li style="list-style: none; margin-left: -30px;"><a href="contact.html" style="text-decoration: none; color:white;"> Contactez-nous </a></li>
                            </ul>
                        </div>
                       
                    </div>
            </div>

            
            <div class="col-md-3 sefoot">
                <div class="row">
                    <h2>Suivez-nous</h2>
                </div>

                <div>
                    <img src="images/tp1.png " alt="" class="log">
                   Aure delice
                </div>
                   
                <div>
                    <img src="images/tp5.png " alt="" class="log">
                    5858585858
 
                </div>
                  
                <div>
                    <img src="images/tp4.png " alt="" class="log">
                    @au_delice
                </div>
                   
                <div>
                    <img src="images/tp3.png " alt="" class="log">
                    98delice
                </div>
                  
                
        </div>
            </div>

        
                <p class="copyr">@Copyrigth 2022 tout droit reserves</p>
           
        </div>
    </div>


    



    <script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="bootstrap.min.js"></script>
<script type="text/javascript" src="scripts.js"></script>
</body>
</html>