

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS only -->
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="bootstrap.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap" rel="stylesheet">
    <style> @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap'); </style>
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body>




      <nav class="navbar navbar-expand-md bg-dark kp">
        <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
        <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
            <i class="fa-solid fa-bars "></i>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="nav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="#">Accueil
                </a>
            </li>
            <li class="nav-item dropdown">
                <a 
                    class="nav-link text-light font-weight-bold px-3 dropdown-toggle" href="#" data-toggle="dropdown">Categorie
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="produit1.php">Chocolat</a>
                    <a class="dropdown-item" href="produit3.php">Miel</a>
                    <a class="dropdown-item" href="produit2.php">Carotte</a>
                </div>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="Apropos.php">A Propos
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="contact.php">Contactez-nous
                </a>
            </li>

        </ul>

            <form class="form-inline ml-3">
                <div class="input-group">
                    <input type="text"   style="border-radius: 8px;" placeholder="">
                    <div>
                        <button type="button " style="background-color:  rgb(255, 255, 13); border-radius: 8px;">Rechercher</button>
                    </div>
                </div>

            </form>
        </div>
    </nav>


    


        <div class="container-fluid ">
           <div class="row hea">
                <div class="col md-11 " style=' background-image: url("images/gateau au miel/depositphotos_35666961-stock-photo-honey-cake"); background-repeat : no-repeat; height: 620px; background-size: 1600px;'>
                <div data-aos="fade-right"
                        data-aos-offset="600"
                        data-aos-easing="ease-in-sine" class="msg">LES MEILLEURS GATEAUX <br> DE AURE DELICE </div>
                        <div data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine"  class="msg1"> Votre satisfaction notre priorité </div>
                </div>
           </div>
           <div class="row">
                <div class="container-fluid">
                    <h1 class="msgp">Meilleur Produit</h1>
                    <p class="msgp1"> Meilleur produit vendu ce week-end !</p>
                </div>
                </div>
                
                <div class="container">
                    <div class="row" >
                        <div data-aos="fade-right"
                        data-aos-offset="400"
                        data-aos-easing="ease-in-sine" class="col-md-3 l3">

                            <div class="row">
                               <a href="produit1.php">  <img   src="images/gateau au chocolat/istockphoto-478348860-612x612.jpg" alt="" class="  w-100  mb-4 imal3">  </a> 
                            </div>
                            <h3 class="titre"> Gâteau au chocolat </h3>
                        </div>
                   
                        <div  data-aos="fade-up"
                        data-aos-anchor-placement="center-center" class="col-md-3 l3">
                            <div class="row">
                                <a href="produit2.php">   <img  src="images/gateau au carotte/istockphoto-152499288-612x612.jpg" alt="" class="w-100  mb-4 imal3">  </a>
                            </div>
                            <h3 class="titre"> Gâteau au carotte </h3>
                        </div>

                        <div data-aos="fade-left"
                        data-aos-offset="400"
                        data-aos-easing="ease-in-sine" class="col-md-3 l3">
                            <div class="row">
                                <a href="produit3.php">  <img src="images/gateau au miel/tag-dessert-3000x2000.jpg" alt="" class="w-100  mb-4 imal3"> </a>
                            </div>
                            <h3 class="titre"> Gâteau au miel </h3>
                        </div>
                       </div>
                       </div> 
                       </div>
                  

                       <div class="container-fluid">
                        <div class="row" >
                        <div data-aos="flip-left" class="col-md-3 l2">
                            <div class="row">
                                <img src="images/photo du template/mt-2266-home-img1.jpg" alt="" class="  w-100 imal2">

                            </div>
                        </div>
                        <div  class="col-md-3 l2">
                            <div class="row">
                                <img src="images/photo du template/mt-2266-blog-img5.jpg" alt="" class="  w-100   imal2">
                            </div>
                        </div>

                        <div data-aos="flip-right" class="col-md-3 l2">
                            <div class="row">
                                <img src="images/photo du template/mt-2266-home-img3.jpg" alt="" class="  w-100   imal2">
                            </div>
                        </div>
                        
                    </div>

              
           </div>


           <div class="container-fluid">
            <h1 class="msgp">Promo du Jour</h1>
            <p class="msgp1">Ne manquez pas nos offres en vedette du jour</p>
        </div>

        <div class="container">
            <div class="row">
                <div  data-aos="fade-right"
                data-aos-offset="400"
                data-aos-easing="ease-in-sine" class="col-md-3 l3">
                    <div class="row">
                        <img src="images/gateau au nutella/59165394-43531203.jpg" alt="" class=" w-100 imal3">

                    </div>
                    <h3 class="titre"> Tarte au nutella </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="promo">17.99$</p>
                                </div>

                                <div class="col-md-6">
                                    <p class="reel">24.99$</p>
                                </div>
                        </div>
                        </div>
                        <button class="panier"><a href="tarteNutella.php" style="text-decoration: none; color:white;">Commander</a> </button>

                    </div>
           
                </div>
                <div class="col-md-3 l3">
                    <div class="row">
                        <img src="images/photo du template/mt-2266-products-img24.jpg" alt="" class=" w-100 imal3">
                    </div>
                    <h3 class="titre"> Gâteau au carotte cremeux</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="promo">23.99$</p>
                                </div>

                                <div class="col-md-6">
                                    <p class="reel">29.99$</p>
                                </div>
                                </div>
                        </div>
                        <button class="panier"><a href="carotteCremeux.php" style="text-decoration: none; color:white;">Commander</a> </button>

                    </div>
                </div>

                <div  data-aos="fade-left"
                data-aos-offset="400"
                data-aos-easing="ease-in-sine" class="col-md-3 l3">
                    <div class="row">
                        <img src="images/gateau au chocolat/istockphoto-904337728-612x612.jpg" alt="" class=" w-100 imal3">
                    </div>
                    <h3 class="titre"> Basic au chocolat </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="promo">14.99$</p>
                                </div>

                                <div class="col-md-6">
                                    <p class="reel">18.99$</p>
                                </div>
                                </div>
                        </div>
                        <button class="panier"><a href="basicChocolat.php" style="text-decoration: none; color:white;">Commander</a> </button>

                    </div>
                </div>
                
            </div>
            </div>

            <div class="container ">
                <h1 class="col-md-12 d-none d-md-block msgp">Produit a Venir</h1>
            </div>
            <div class="container">
                <div class="col-md-12 d-none d-md-block">
                    <div class="row">
                <div class="col-md-7 ">
                        <img src="images/gateau au miel/i76592-cake-aux-amandes.jpg" alt="" class=" imal4">
        </div>
        <div class="col-md-3 ">
            <div class="row">
                <img style="width: 100% ;" src="images/gateau au chocolat/chocolat.jpg" alt="" class="lou">
                </div>
            <div class="row">
                <img style="width: 93% ;" src="images/gateau au miel/gâteau-léger-au-miel.jpg"  alt="" class=" lou">
            </div>

        </div>
        </div>
        </div>
        </div>
        
        <div class="container-fluid ">
            <div class="row">
            <div data-aos="zoom-out" class="col-md-3 l5">
                <div class="row ">
                    <img style="height: 80px; width: 50px; " src="images/photo du template/mt-2266-home-img5.png" alt="" class=" ima">
                </div>
                <h3 class="titre2"> Pack de mariage </h3>
                <div class="row">
                    <div class="col-md-12">
                           <p class="txt">
                           Dans le pack de mariage, vous trouverez des gâteaux préparés aux formes variées spécialement fait pour toute sorte de cérémonie de mariage sans pareille .
                           </p>
                    </div>
                    <button class="panier1"> <a href="formulairePack.php" style="text-decoration: none; color:white;"> Commandez ici </a> </button>
    
                </div>
            </div>
    
            <div data-aos="zoom-in" class="col-md-3 l5">
                <div class="row olo">
                    <img   style="height: 80px; width: 50px; " src="images/photo du template/mt-2266-home-img5.png" alt="" class="ima">
                </div>
                <h3 class="titre2"> Pack d'anniversaire </h3>
                <div class="row">
                    <div class="col-md-12">
                           <p class="txt">
                           Dans le pack d'anniversaire, vous trouverez des gâteaux préparés au formes variées spécialement fait pour des évènements d'anniversaire de toute sorte.
                           </p>
                    </div>
                    <button class="panier1"> <a href="formulairePack.php" style="text-decoration: none; color:white;"> Commandez ici </a> </button>
    
                </div>
            </div>
    
            <div data-aos="zoom-out" class="col-md-3 l5">
                <div class="row olo">
                    <img  style="height: 80px; width: 50px; "  src="images/photo du template/mt-2266-home-img5.png" alt="" class="ima">
                </div>
                <h3 class="titre2">Pack Administratif </h3>
                <div class="row">
                    <div class="col-md-12">
                           <p class="txt">
                           Dans ce pack, vous trouverez des gâteaux préparés au formes variées spécialement fait pour des célébrations administrative de toute sorte sans  pareille de ouf.
                            </p>
                    </div>
                    <button class="panier1"> <a href="formulairePack.php" style="text-decoration: none; color:white;"> Commandez ici </a> </button>
    
                </div>
            </div>
    
            </div>
        </div>
       

        

        <div class="container-fluid">
            <h1 class="msgp">Notre Blog</h1>
           
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4 l6">
                    <div class="row">
                        <img src="images/photo du template/mt-2266-blog-img4.jpg" alt="" class="w-100 imal6">

                    </div>
                    </div>


                    <div class="col-md-6 l6p">
                        <h3 class="titrel6"> Tarte au Nutella </h3>
                        <p>
                        Les Tartes au Nutella sont des beignets très intéressants à manger avec une saveur irrésistible qui vous donneront certainement l'appétit.
                        Ces tartes au nutella sont d'une importance capitale car elles vous faciliteront la vie. En plus elles sont conseillées pour le dessert.
                        Dans notre blog nous vous proposons la procédure pour ne pas dire la recette pour préparer ces tartes aux nutella.
                        Nous allons vous expliquer de font en comble comment bien faire ces tartes pour votre plus grand plaisir avec tous les détails nécessaires.
                        Notre objectif est de vous montrer tous les secrets qui se cache dans la préparation des tartes au nutella.
                        Nous vous invitons a consomme les tartes aux nutella pour votre bien.
                        Ainsi nous vous exhortons a lire attentivement notre blog pour la bonne préparation de vos tartes au nutella. 
                        </p>
                        <button class="panier"> <a href="blog1.php" style="text-decoration: none; color:white;"> Lire la suite </a> </button>
                    </div>
                    </div>
                    </div>
                    

                    <div class="container">
                        <div class="row">
                        <div class="col-md-6 d-none d-md-block l6pp">
                            <h3 class="titrel6"> Gateau au noix de Muscade </h3>
                            <p>
                            Les Gâteau au Noix de Muscade sont des gâteaux fais a essentiellement à base de noix de muscade. Ils sont d'une saveur unique avec très leurs odeurs appétissantes. 
                            Ces gâteux sont d'une importance capitale car elles vous faciliteront la vie. Surtout qu'ils vont vous éviter les réflexions à savoir quoi prendre pour le dessert.
                            Dans notre blog nous vous proposons la procédure pour ne pas dire la recette pour préparer ces gâteaux aux noix de muscade.
                            Nous allons vous expliquer de font en comble comment bien faire ces gâteaux pour votre plus grand plaisir avec tous les détails nécessaires.
                            Notre objectif est de vous montrer tous les secrets qui se cache dans la préparation de ces gâteaux.
                            Ainsi nous vous exhortons a lire attentivement notre blog pour la bonne préparation de vos gâteaux au noix de muscade. 
                            </p>
                            <button class="panier"> <a href="blog2.php" style="text-decoration: none; color:white;"> Lire la suite </a></button>
                        </div>

                        <div class="col-md-4 d-none d-md-block l66">
                            <div class="row">
                                <img src="images/gateau au nutella/59165394-43531203.jpg" alt="" class="w-100 imal6">
        
                            </div>
                            </div>
                            </div>
        </div>
        
                  
        





        <div class="conttainer-fluid">
            <div class="col-md-12 foot">
                <div class="row">
                    <div class="col-md-3 sefoot">
                            <div class="row">
                                <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
                            </div>
                            <div class="row">
                                <p ">
                                    Une pâtisserie qui vous fait goute
                                    aux délices de l'art culinaire (pâtisserie) et qui satisfait tous vos fantasme sur l'art culinaire de la pâtisserie
                                    pour votre plus grand plaisir.

                                </p>
                               
                            </div>
                    </div>
    
                    <div class="col-md-3 sefoots">
                        <div class="row">
                            <h2>Liens rapide</h2>
                        </div>
                        <div class="row">
                            <div class="dernier">
                                <ul >
                                    <li style="list-style: none; margin-left: -30px;"> <a href="#" style="text-decoration: none; color:white;"> Accueil </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="inscription.php" style="text-decoration: none; color:white;"> Inscription </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="connexion.php" style="text-decoration: none; color:white;"> connexion </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="contact.html" style="text-decoration: none; color:white;"> Contactez-nous </a></li>
                                </ul>
                            </div>
                           
                        </div>
                </div>
    
                
                <div class="col-md-3 sefoot">
                    <div class="row">
                        <h2>Suivez-nous</h2>
                    </div>
    
                    <div>
                        <img src="images/tp1.png " alt="" class="log">
                       Aure delice
                    </div>
                       
                    <div>
                        <img src="images/tp5.png " alt="" class="log">
                        5858585858
     
                    </div>
                      
                    <div>
                        <img src="images/tp4.png " alt="" class="log">
                        @au_delice
                    </div>
                       
                    <div>
                        <img src="images/tp3.png " alt="" class="log">
                        98delice
                    </div>
                      
                    
            </div>
                </div>
    
            
                    <p class="copyr">@Copyrigth 2022 tout droit réserves</p>
               
            </div>
        </div>
    
    






           
        <script>
            AOS.init();
          </script>
 

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="bootstrap.min.js"></script>
<script type="text/javascript" src="scripts.js"></script>

<script>
    AOS.init();
  </script>
</body>
</html>