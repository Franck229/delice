<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS only -->
<link rel="stylesheet" href="bootstrap.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap" rel="stylesheet">
    <style> @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap'); </style>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="blog.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body>
    

<nav class="navbar navbar-expand-md bg-dark">
        <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
        <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
            <i class="fa-solid fa-bars "></i>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="nav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="index.php">Accueil
                </a>
            </li>
            <li class="nav-item dropdown">
                <a 
                    class="nav-link text-light font-weight-bold px-3 dropdown-toggle" href="#" data-toggle="dropdown">Categorie
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="produit1.php">Chocolat</a>
                    <a class="dropdown-item" href="produit3.php">Miel</a>
                    <a class="dropdown-item" href="produit2.php">Carotte</a>
                </div>
            </li>
          
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="Apropos.php">A Propos
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="contact.php">Contactez-nous
                </a>
            </li>

        </ul>

            <form class="form-inline ml-3">
                <div class="input-group">
                    <input type="text"   style="border-radius: 8px;" placeholder="">
                    <div>
                        <button type="button " style="background-color: rgb(255, 255, 13); border-radius: 8px;">Rechercher</button>
                    </div>
                </div>

            </form>
        </div>
    </nav>




    <div class="container">

        <h2 class="tblog">GATEAUX AU NOIX DE COCO</h2>
        <p class="pblog">Les gâteaux a la noix de coco sont des beignets très intéressant a manger avec une saveur irrésistible qui vous donneront certainement l'appétit.
            Nous vous invitons a consomme les gâteaux a la noix de coco pour votre bien. Ainsi nous vous proposons une recette pour vous apprendre à préparer ce gâteau.

        </p>

    </div>

    <div class="container-fluid">
        <div class="col-md-12">
        <div class="row">
            <div class="col-md-2 pop">
                <h4>Recents post</h4>
                <a href="blog2.php"> <img src="images/gateau au nutella/59165394-43531203.jpg" alt=""  class=" w-100 h-5 imblog"> </a>
                <h6>Gateau au noix de muscades</h6>

                <a href="blog1.php"> <img src="images/photo du template/mt-2266-blog-img4.jpg" alt="" class=" w-100 imblog" > </a>
                <h6>Tarte au nutella</h6>
            </div>
        
            <div class="col-md-8 toutb">
                <div class="row">
                    <div class="col-md-5">
                        
                        <img src="images/gateau au miel/istockphoto-451147547-170667a-gateau-yaourt.jpg" alt="" class=" im " >
                    </div>
                    
                    <div class="col-md-7">
                    <p class=""><b> Pour ce faire , nous avons besoin de : </b><br>
                                    - 200g de noix de coco rapee <br>
                                    - 150g de sucre <br>
                                    - 100g de beurre fondu <br>
                                    - 4 oeufs <br>
                                    - 1 cuillere a soupe d'extrait de vanille <br>
                                    - 1 pincee de sel <br>
                                    - 1 sachet de levure chimique <br><br>

                                    <b> Instructions:</b> <br>
                                    Préchauffez le four à 180 degrés. <br>
                                    Dans un grand bol, mélangez la noix de coco râpée, le sucre, la levure et la pincée de sel. <br>
                                    Ajouter le beurre fondu et l'extrait de vanille, puis mélangez bien. <br>
                                    Battez les œufs dans un autre bol, puis incorporez-les petit a petit <br>
                    </div>
                    </div>
                    <p class="truc">
                                    Dans le mélange de noix de coco en remuant bien entre chaque ajout. <br>
                                    Versez le mélange dans un moule a gâteaux préalablement beurre et farine. <br>
                                    Enfournez pendant environ 28 minutes, jusqu'à ce que le gâteau soit dore et que la pointe du couteau ressorte sèche. <br>
                                    Laissez refroidir le gâteau avant de le servir. <br>
                                    Si vous le souhaitez, vous pouvez également ajouter une garniture de votre choix, comme des pépites de chocolat ou des fruits confits. 
                                    Vous pouvez également utiliser de la noix de coco fraiche râpée a la place de la noix de coco séchée. <br><br>
                                    <b>Bon appétit ! </b>.


                </div>




               
                
            </div>
            </div>

        </div>





        <div class="conttainer-fluid">
            <div class="col-md-12 foot">
                <div class="row">
                    <div class="col-md-3 sefoot">
                            <div class="row">
                                <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
                            </div>
                            <div class="row">
                                <p ">
                                    Une patisserie qui vous fait goute
                                    aux delices de l'art culinaire (patisserie) et qui satisfait tout vos fantasme sur l'art culinaire de la patisserie
                                     pour votre plus grand plaisir.
                                </p>
                               
                            </div>
                    </div>
    
                    <div class="col-md-3 sefoots">
                        <div class="row">
                            <h2>Liens rapide</h2>
                        </div>
                        <div class="row">
                            <div class="dernier">
                                <ul >
                                    <li style="list-style: none; margin-left: -30px;"> <a href="#" style="text-decoration: none; color:white;"> Accueil </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="inscription.php" style="text-decoration: none; color:white;"> Inscription </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="connexion.php" style="text-decoration: none; color:white;"> connexion </a></li>
                                    <li style="list-style: none; margin-left: -30px;"><a href="contact.html" style="text-decoration: none; color:white;"> Contactez-nous </a></li>
                                </ul>
                            </div>
                           
                        </div>
                </div>
    
                
                <div class="col-md-3 sefoot">
                    <div class="row">
                        <h2>Suivez-nous</h2>
                    </div>
    
                    <div>
                        <img src="images/tp1.png " alt="" class="log">
                       Aure delice
                    </div>
                       
                    <div>
                        <img src="images/tp5.png " alt="" class="log">
                        5858585858
     
                    </div>
                      
                    <div>
                        <img src="images/tp4.png " alt="" class="log">
                        @au_delice
                    </div>
                       
                    <div>
                        <img src="images/tp3.png " alt="" class="log">
                        98delice
                    </div>
                      
                    
            </div>
                </div>
    
            
                    <p class="copyr">@Copyrigth 2022 tout droit reserves</p>
               
            </div>
        </div>
    
    














    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts.js"></script>
    </body>
    </html>