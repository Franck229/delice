/**
 * Serialization
 */
 $.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).on("click", "#ajouter", function(e) {
    e.preventDefault();

    var form = $("#form_add");

    if ($('#code').val() === "" || $('#libelle').val() === "") {
        $('#err').css("display", "block");

        return
    }

    // get the serialized properties and values of the form
    var form_data = form.serializeObject();

    $.ajax({
        url: '../controller/addCategorie.php',
        type: 'POST',
        dataType: 'json',
        data: form_data //,

    });

    document.location.reload();
});

$.getJSON("../controller/allCategorie.php", function(result) {
    for (var i = 0; i < result.length; i++) {
        let trTag = $("<tr id='row' data-code=" + result[i]["code"] + " data-libelle=" + result[i]["libelle"] + "></tr>");
        $("#table").append(trTag);
        $(trTag).append("<td>" + result[i]["code"] + "</td>");
        $(trTag).append("<td>" + result[i]["libelle"] + "</td>");
        $(trTag).append("<td><button id='edit' type='button' class='mx-1 btn btn-sm btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal'>Editer</button><a href='../controller/delCategorie.php?code=" + result[i]["code"] + "' class='btn btn-sm btn-danger'>Supprimer</a></td>");

        $("#code_code").append("<option value=" + result[i]["libelle"] + ">" + result[i]["libelle"] + "</option>");
    }

});










/**
 * Serialization
 */


$(document).on("click", "#ajouter", function(e) {
    e.preventDefault();

    var form = $("#form_ad");

    if ($('#ref').val() === "" || $('#libelle').val() === "" || $('#prix').val() === "" || $('#qteStock').val() === "" || $('#code_code').val() === "" ) {
        $('#err').css("display", "block");

        return
    }

    // get the serialized properties and values of the form
    var form_data = form.serializeObject();

    $.ajax({
        url: '../controller/addProduit.php',
        type: 'POST',
        dataType: 'json',
        data: form_data //,

    });

    document.location.reload();
});

$.getJSON("../controller/allProduit.php", function(result) {
    for (var i = 0; i < result.length; i++) {
        let trTag = $("<tr id='row' data-ref=" + result[i]["ref"] + "data-libelle=" + result[i]["libelle"]  + " data-prix=" + result[i]["prix"] +  " data-qteStock=" + result[i]["qteStock"] +  " data-code=" + result[i]["code"] + "></tr>");
        $("#table1").append(trTag);
        $(trTag).append("<td>" + result[i]["ref"] + "</td>");
        $(trTag).append("<td>" + result[i]["libelle"] + "</td>");
        $(trTag).append("<td>" + result[i]["prix"] + "</td>");
        $(trTag).append("<td>" + result[i]["qteStock"] + "</td>");
        $(trTag).append("<td>" + result[i]["code"] + "</td>");
        $(trTag).append("<td><button id='edit' type='button' class='mx-1 btn btn-sm btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal'>Editer</button><a href='../controller/delProduit.php?ref=" + result[i]["ref"] + "' class='btn btn-sm btn-danger'>Supprimer</a></td>");
        $("#ref_ref").append("<option value=" + result[i]["libelle"] + ">" + result[i]["libelle"] + "</option>");
    }

});



















$(document).on("click", "#ajouter", function(e) {
    e.preventDefault();

    var form = $("#form_addentree");

    if ($('#numero').val() === "" || $('#dateentree').val() === ""  || $('#montot').val() === "") {
        $('#err').css("display", "block");

        return
    }

    // get the serialized properties and values of the form
    var form_data = form.serializeObject();

    $.ajax({
        url: '../controller/addEntree.php',
        type: 'POST',
        dataType: 'json',
        data: form_data //,

    });

    document.location.reload();
});

$.getJSON("../controller/allEntree.php", function(result) {
    for (var i = 0; i < result.length; i++) {
        let trTag = $("<tr id='row' data-numero=" + result[i]["numero"] + " data-dateentree=" + result[i]["dateentree"] + " data-montot=" + result[i]["montot"] +"></tr>");
        $("#tableentree").append(trTag);
        $(trTag).append("<td>" + result[i]["numero"] + "</td>");
        $(trTag).append("<td>" + result[i]["dateentree"] + "</td>");
        $(trTag).append("<td>" + result[i]["montot"] + "</td>");
        $(trTag).append("<td><button id='edit' type='button' class='mx-1 btn btn-sm btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal'>Editer</button><a href='../controller/delEntree.php?numero=" + result[i]["numero"] + "' class='btn btn-sm btn-danger'>Supprimer</a></td>");

        $("#numero_numero").append("<option value=" + result[i]["numero"] + ">" + result[i]["numero"] + "</option>");
    }

});
















$(document).on("click", "#ajouter", function(e) {
    e.preventDefault();

    var form = $("#form_adddetail");

    if ($('#qteentree').val() === "" || $('#numero_numero').val() === ""  || $('#ref_ref').val() === "") {
        $('#err').css("display", "block");

        return
    }

    // get the serialized properties and values of the form
    var form_data = form.serializeObject();

    $.ajax({
        url: '../controller/addDetailentrer.php',
        type: 'POST',
        dataType: 'json',
        data: form_data //,

    });

    document.location.reload();
});

$.getJSON("../controller/allDetailentrer.php", function(result) {
    for (var i = 0; i < result.length; i++) {
        let trTag = $("<tr id='row' data-qteentree=" + result[i]["qteentree"] + " data-numero=" + result[i]["numero"] +  " data-ref=" + result[i]["ref"] + "></tr>");
        $("#tabledetail").append(trTag);
        $(trTag).append("<td>" + result[i]["qteentree"] + "</td>");
        $(trTag).append("<td>" + result[i]["numero"] + "</td>");
        $(trTag).append("<td>" + result[i]["ref"] + "</td>");
        $(trTag).append("<td><button id='edit' type='button' class='mx-1 btn btn-sm btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal'>Editer</button><a href='../controller/delDetailentrer.php?numero=" + result[i]["numero"] + "' class='btn btn-sm btn-danger'>Supprimer</a></td>");
    }

});

