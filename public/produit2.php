<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS only -->
<link rel="stylesheet" href="bootstrap.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap" rel="stylesheet">
    <style> @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@1,200;1,400;1,800&display=swap'); </style>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="produit1.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body>
<nav class="navbar navbar-expand-md bg-dark">
        <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
        <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
            <i class="fa-solid fa-bars "></i>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="nav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="index.php">Accueil
                </a>
            </li>
            <li class="nav-item dropdown">
                <a 
                    class="nav-link text-light font-weight-bold px-3 dropdown-toggle" href="#" data-toggle="dropdown">Categorie
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="produit1.php">Chocolat</a>
                    <a class="dropdown-item" href="produit3.php">Miel</a>
                    <a class="dropdown-item" href="produit2.php">Carotte</a>
                </div>
            </li>
          
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="Apropos.php">A Propos
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link text-light font-weight-bold px-3" href="contact.php">Contactez-nous
                </a>
            </li>

        </ul>

            <form class="form-inline ml-3">
                <div class="input-group">
                    <input type="text"   style="border-radius: 8px;" placeholder="">
                    <div>
                        <button type="button " style="background-color:  rgb(255, 255, 13); border-radius: 8px;">Rechercher</button>
                    </div>
                </div>

            </form>
        </div>
    </nav>

    <div class="container">
        <div class="row" >
            <div class="col-md-4 p1">
                <div class="row">
                    <img  src="images/gateau au carotte/istockphoto-152499288-612x612.jpg" alt="" class="  w-100  mb-4 imap1">
                </div>
                </div>
                <div class="col-md-4 p11">
                    <h2 class="tp1"> Gâteau au Carrotte </h1>
                    <p class="promo">21.99$</p>
                    <h5 class="tp1">Type : Gâteau au Carrotte</h5>
                    <h5 class="tp1">Sku : 0012</h5>
                    <h5 class="tp1">Vendeur : Benin</h5>
                    <h5 class="tp1">Quantité :</h5>
                    <input type="text" class="tp1">
                    <h5 class="tp1">Quantité disponible : 10</h5>
                    <button class="panier"><a href="carotte.php" style="text-decoration: none; color:white;">Commander</a> </button>

                </div>
                </div>
                </div>
                <div class="container">
                    <h3 class="desc">Description sur la livraison</h3>
                    <p class="pp1">
                        Pour la livraison de nos produits, nous prenons 1,5$ sur le prix initial du produit.
                        Tout au long du processus de la livraison, nous assurons la conservation du produit. 
                        Nous livrons nos produits uniquement dans les capitales du pays. 
                        Nous disposons de livreur des livreurs de très bonne qualité en qui nous avons une 
                        Confiance absolue et qui sont très rapide et efficace. La ponctualité est notre mot d'ordre. 
                        Du coup si vous vous trouvez en dehors des capitales, nous vous prions de bien vouloir 
                        vous déplacer pour prendre vos produits. Merci pour votre fidélité envers notre pâtisserie.
                        Votre satisfaction notre priorité.

                        
                    </p>
    
                    <h3 class="desc">Produits relatifs</h3>

                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-3 l3">
                            <div class="row">
                                <img src="images/gateau au nutella/59165394-43531203.jpg" alt="" class=" w-100 imal3">
        
                            </div>
                            <h3 class="titre"> Tarte au nutella </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="promo">17.99$</p>
                                        </div>
        
                                        <div class="col-md-6">
                                            <p class="reel">24.99$</p>
                                        </div>
                                </div>
                                </div>
                                <button class="panier"><a href="tarteNutella.php" style="text-decoration: none; color:white;">Commander</a> </button>        
                            </div>
                   
                        </div>
                        <div class="col-md-3 l3">
                            <div class="row">
                                <img src="images/photo du template/mt-2266-products-img24.jpg" alt="" class=" w-100 imal3">
                            </div>
                            <h3 class="titre"> Gâteau au carotte cremeux</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="promo">23.99$</p>
                                        </div>
        
                                        <div class="col-md-6">
                                            <p class="reel">29.99$</p>
                                        </div>
                                        </div>
                                </div>
                                <button class="panier"><a href="carotteCremeux.php" style="text-decoration: none; color:white;">Commander</a> </button>        
                            </div>
                        </div>
        
                        <div class="col-md-3 l3">
                            <div class="row">
                                <img src="images/gateau au chocolat/istockphoto-904337728-612x612.jpg" alt="" class=" w-100 imal3">
                            </div>
                            <h3 class="titre"> Basic au chocolat </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="promo">14.99$</p>
                                        </div>
        
                                        <div class="col-md-6">
                                            <p class="reel">18.99$</p>
                                        </div>
                                        </div>
                                </div>
                                <button class="panier"><a href="chocolatBasic.php" style="text-decoration: none; color:white;">Commander</a> </button>        
                            </div>
                        </div>
                        
                    </div>
                    </div>
        
               

                    <div class="conttainer-fluid">
                        <div class="col-md-12 foot">
                            <div class="row">
                                <div class="col-md-3 sefoot">
                                        <div class="row">
                                            <a href="#" style="text-decoration: none; font-weight:bold; color: rgb(255, 255, 13);font-size: 22px;">Aure Delice</a >
                                        </div>
                                        <div class="row">
                                            <p ">
                                            Une pâtisserie qui vous fait goute
                                            aux délices de l'art culinaire (pâtisserie) et qui satisfait tous vos fantasme sur l'art culinaire de la pâtisserie
                                            pour votre plus grand plaisir.
                                            </p>
                                           
                                        </div>
                                </div>
                
                                <div class="col-md-3 sefoots">
                                    <div class="row">
                                        <h2>Liens rapide</h2>
                                    </div>
                                    <div class="row">
                                        <div class="dernier">
                                            <ul >
                                                <li style="list-style: none; margin-left: -30px;"> <a href="#" style="text-decoration: none; color:white;"> Accueil </a></li>
                                                <li style="list-style: none; margin-left: -30px;"><a href="inscription.php" style="text-decoration: none; color:white;"> Inscription </a></li>
                                                <li style="list-style: none; margin-left: -30px;"><a href="connexion.php" style="text-decoration: none; color:white;"> connexion </a></li>
                                                <li style="list-style: none; margin-left: -30px;"><a href="contact.html" style="text-decoration: none; color:white;"> Contactez-nous </a></li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                            </div>
                
                            
                            <div class="col-md-3 sefoot">
                                <div class="row">
                                    <h2>Suivez-nous</h2>
                                </div>
                
                                <div>
                                    <img src="images/tp1.png " alt="" class="log">
                                   Aure delice
                                </div>
                                   
                                <div>
                                    <img src="images/tp5.png " alt="" class="log">
                                    5858585858
                 
                                </div>
                                  
                                <div>
                                    <img src="images/tp4.png " alt="" class="log">
                                    @au_delice
                                </div>
                                   
                                <div>
                                    <img src="images/tp3.png " alt="" class="log">
                                    98delice
                                </div>
                                  
                                
                        </div>
                            </div>
                
                        
                                <p class="copyr">@Copyrigth 2022 tout droit reserves</p>
                           
                        </div>
                    </div>
                





 


<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="bootstrap.min.js"></script>
<script type="text/javascript" src="scripts.js"></script>
</body>
</html>   
